-- MariaDB dump 10.18  Distrib 10.4.17-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: capstonedb
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `body_system`
--

DROP TABLE IF EXISTS `body_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `body_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `body_system`
--

LOCK TABLES `body_system` WRITE;
/*!40000 ALTER TABLE `body_system` DISABLE KEYS */;
INSERT INTO `body_system` VALUES (1,'CIRCULATORY','2021-04-15',NULL),(2,'DIGESTIVE & INTESTINAL','2021-04-15',NULL),(3,'GLANDULAR & ENDOCRINE','2021-04-15',NULL),(4,'HEPATIC','2021-04-15',NULL),(5,'IMMUNE & LYMPHATIC','2021-04-15',NULL),(6,'INTEGUMENTARY','2021-04-15',NULL),(7,'MUSCULAR & SKELETAL','2021-04-15',NULL),(8,'NERVOUS','2021-04-15',NULL),(9,'REPRODUCTIVE','2021-04-15',NULL),(10,'RESPIRATORY','2021-04-15',NULL),(11,'URINARY & RENAL','2021-04-15',NULL);
/*!40000 ALTER TABLE `body_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capstone_order`
--

DROP TABLE IF EXISTS `capstone_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capstone_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `total_tax` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_fk` (`user_id`),
  CONSTRAINT `order_fk` FOREIGN KEY (`user_id`) REFERENCES `capstone_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capstone_order`
--

LOCK TABLES `capstone_order` WRITE;
/*!40000 ALTER TABLE `capstone_order` DISABLE KEYS */;
INSERT INTO `capstone_order` VALUES (1,1,'2021-04-15',106.80,2.27,109.07),(2,2,'2021-04-15',102.75,2.50,105.25),(3,3,'2021-04-15',32.45,NULL,32.45),(4,4,'2021-04-15',19.15,NULL,19.15),(5,5,'2021-04-15',33.05,NULL,33.05);
/*!40000 ALTER TABLE `capstone_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capstone_user`
--

DROP TABLE IF EXISTS `capstone_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capstone_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_id` varchar(15) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `province` varchar(100) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `member_code` varchar(15) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capstone_user`
--

LOCK TABLES `capstone_user` WRITE;
/*!40000 ALTER TABLE `capstone_user` DISABLE KEYS */;
INSERT INTO `capstone_user` VALUES (1,'1001787124','GALO FERNANDO','PALACIOS MEDINA','GRAL ENRIQUEZ','ATUNTAQUI','IMBABURA','2909732',NULL,'41348','1001787124','2021-04-15',NULL),(2,'1000205698','MARIA REBECA','OBANDO GUZMAN','CALLE JUAN MIGUEL','IBARRA','IMBABURA','952661',NULL,'48600','1000205698','2021-04-15',NULL),(3,'1001207404','ROSA MARIA','LUNA JATIVA','SUCRE 876','IBARRA','IMBABURA','606071',NULL,'46824','1001207404','2021-04-15',NULL),(4,'1003020235','MONICA PATRICIA','LOPEZ OBREGON','GARCIA MORENO Y CHICA NARVAEZ','IBARRA','IMBABURA','643486',NULL,'44388','1003020235','2021-04-15',NULL),(5,'1001020435','MARIA GUADALUPE','ALDAS','EL EJIDO DE CARANQUI','IBARRA','IMBABURA','091134671',NULL,'51623','1001020435','2021-04-15',NULL);
/*!40000 ALTER TABLE `capstone_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `total_tax` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_detail_fk_or` (`order_id`),
  KEY `order_detail_fk_pr` (`product_id`),
  CONSTRAINT `order_detail_fk_or` FOREIGN KEY (`order_id`) REFERENCES `capstone_order` (`id`),
  CONSTRAINT `order_detail_fk_pr` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` VALUES (1,1,1,3,14.95,NULL,44.85),(2,1,3,3,14.35,NULL,43.05),(3,1,14,1,18.90,2.27,21.17),(4,2,15,1,20.80,2.50,23.30),(5,2,16,1,42.60,NULL,42.60),(6,2,17,1,20.20,NULL,20.20),(7,2,18,1,19.15,NULL,19.15),(8,3,20,1,32.45,NULL,32.45),(9,4,13,1,19.15,NULL,19.15),(10,5,10,1,33.05,NULL,33.05);
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image` varchar(100) NOT NULL,
  `body_system_id` int(11) NOT NULL,
  `product_form_id` int(11) NOT NULL,
  `min_quantity` int(11) NOT NULL,
  `stock_on_hand` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `member_price` decimal(10,2) NOT NULL,
  `regular_price` decimal(10,2) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_fk_bs` (`body_system_id`),
  KEY `product_fk_pf` (`product_form_id`),
  CONSTRAINT `product_fk_bs` FOREIGN KEY (`body_system_id`) REFERENCES `body_system` (`id`),
  CONSTRAINT `product_fk_pf` FOREIGN KEY (`product_form_id`) REFERENCES `product_form` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Calcium-Magnesium','3244','Vital Nutrient for Bones, Muscles, Ligaments, Tendons, and Skin','calcium–magnesium.png',1,15,5,20,11.95,14.95,19.95,'2021-04-15',NULL),(2,'Ionic Minerals','310','Charged Ionic Minerals with Acai and Fulvic Acid','ionic-minerals.png',1,8,5,20,24.94,31.35,39.20,'2021-04-15',NULL),(3,'Cascara Sagrada','170','Improves Elimination','cascara-sagrada.png',2,2,5,20,13.45,14.35,17.95,'2021-04-15',NULL),(4,'Psyllium Hulls Combination','1376','Intestinal System Support. Is an excellent source of soluble fiber. Facilitates the natural process of waste elimination. May support cholesterol levels already in the normal range','Psyllium-Hulls-Combination.png',2,13,5,20,23.95,25.55,31.95,'2021-04-15',NULL),(5,'Energ-V','875','Promotes energy. Provides circulatory, thyroid, and nervous system support.','Energ-V.png',3,2,5,20,18.45,19.65,24.60,'2021-04-15',NULL),(6,'SugarReg','927','Supports the glandular system. Supports already normal-range blood sugar levels. Activates cell glucose transporters.','SugarReg.png',3,2,5,20,25.95,27.65,34.60,'2021-04-15',NULL),(7,'LIV-J','1011','Supports the digestive and hepatic systems. Nourishes the liver.','LIV-J.png',4,2,5,20,18.55,19.80,24.75,'2021-04-15',NULL),(8,'Mood Elevator (Chinese)','1878','Contains herbs traditionally used for nervous system support. Traditionally used to uplift spirits, improve vitality and support occasional sleeplessness','Mood-Elevator.png',4,16,5,20,23.05,24.60,30.75,'2021-04-15',NULL),(9,'Nature’s Noni','4066','Structural, immune, and digestive system support. It is useful for promoting healthy joints. Antioxidant protection. Helps support digestion','Natures-Noni.png',5,8,5,20,23.55,25.15,31.45,'2021-04-15',NULL),(10,'Super Algae','1056','Stimulates the immune system. Is rich in essential minerals.','Super-Algae.png',5,13,5,20,30.95,33.05,41.30,'2021-04-15',NULL),(11,'Probiotic Power (Sunshine Heroes)','3346','Supports the digestive and intestinal systems. Aids digestion and intestinal function. Strengthens immune response.','Probiotic-Power.png',6,3,5,20,18.50,19.75,24.70,'2021-04-15',NULL),(12,'Silver Shield Rescue Gel (24 ppm)','4953','It may help inhibit the growth of microorganisms within the dressing. Ideal for the topical management of minor cuts, lacerations, abrasions, skin irritations and 1st and 2nd degree burns.','Silver-Shield-Rescue-Gel.png',6,6,5,20,13.40,14.30,17.90,'2021-04-15',NULL),(13,'Pau d\' \'Arco Lotion','21912','Moisturizes and soothes the skin. Non-greasy. Absorbed quickly. Free from parabens, fragrances and dyes. Safety, allergy and dermatologist-tested','Paud-Arco-Lotion.png',6,9,5,20,17.95,19.15,23.95,'2021-04-15',NULL),(14,'Jojoba Oil','1695','Restores luster to dry or damaged hair. Protects hair against damage.','Jojoba-Oil.png',6,9,5,20,17.70,18.90,23.65,'2021-04-15',NULL),(15,'Tei-Fu Recovery Massage Lotion','21913','Provides warming, penetrating pain relief for sore muscles and joints. May provide temporary relief of minor aches and pains associated with backache, strains, sprains and bruises. Supports improved physical recovery','Tei-Fu-Recovery-Massage-Lotion.png',7,9,5,20,19.50,20.80,26.00,'2021-04-15',NULL),(16,'Blood Stimulator TCM Conc.','1005','Enhances bodily function by improving blood quality and circulation. Helps strengthen the immune system. Soothes the nerves and muscles. Encourages an overall feeling of well-being. Helps maintain healthy skin complexion. Supports the circulatory system.','Blood-Stimulator-TCM.png',7,2,5,20,39.95,42.60,53.30,'2021-04-15',NULL),(17,'Spirulina','681','With plant-based, complete protein to fuel your body. Provides traditional support for the nervous system. Non-GMO and free from preservatives and dyes 800 mg of blue-green microalgae per serving','Spirulina.png',8,2,5,20,18.95,20.20,25.30,'2021-04-15',NULL),(18,'Melatonin Extra','2830','Improves Elimination','Melatonin-Extra.png',8,2,5,20,17.95,19.15,23.95,'2021-04-15',NULL),(19,'Nature\'s\' Prenatal','3242','Provides essential nutrients needed for energy and metabolism during pregnancy and lactation. Provides 800 mcg folic acid needed to prevent neural tube defects. Contains antioxidants to help fight the damaging effects of free radicals.','Natures-Prenatal.png',9,15,5,20,22.50,24.00,30.00,'2021-04-15',NULL),(20,'Lung Support TCM Concentrate','1004','Improves Elimination','Lung-Support TCM-Concentrate.png',10,2,5,20,30.45,32.45,40.60,'2021-04-15',NULL),(21,'Liver Balance TCM Concentrate','1008','Improves Elimination','Liver-Balance-TCM-Concentrate.png',11,2,5,20,26.95,28.75,35.95,'2021-04-15',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_form`
--

DROP TABLE IF EXISTS `product_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_form`
--

LOCK TABLES `product_form` WRITE;
/*!40000 ALTER TABLE `product_form` DISABLE KEYS */;
INSERT INTO `product_form` VALUES (1,'Bottle','2021-04-15',NULL),(2,'Capsules','2021-04-15',NULL),(3,'Chewable Tablets','2021-04-15',NULL),(4,'Cream','2021-04-15',NULL),(5,'Dropper','2021-04-15',NULL),(6,'Gel','2021-04-15',NULL),(7,'Gummies','2021-04-15',NULL),(8,'Liquid','2021-04-15',NULL),(9,'Lotion','2021-04-15',NULL),(10,'Pack','2021-04-15',NULL),(11,'Packets','2021-04-15',NULL),(12,'Pieces','2021-04-15',NULL),(13,'Powder','2021-04-15',NULL),(14,'Softgel Capsules','2021-04-15',NULL),(15,'Tablets','2021-04-15',NULL),(16,'VegieCaps','2021-04-15',NULL);
/*!40000 ALTER TABLE `product_form` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-15 20:06:18
